<?php
class Query {
    protected $dbc;
    function __construct($dbc){
        $this->dbc = $dbc;
    }
//פונקציה שתקבל משתנה שהוא השאילתא והפונקציה תריץ את השאילתא מול הDB
    public function query($query){
        if($result = $this->dbc->query($query)) {
            return $result;
        } else {
            echo "something went wrong with the query";
        }

    }
}
///
///
?>