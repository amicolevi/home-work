<?php
 class Htmalpage {
    protected $title = "first hw";
    protected $body = "My first home work";
     public function view(){
         echo "<html><head><title>$this->title</title></head><body> $this->body</body></html>";
     }
     function __construct($title = "", $body = ""){
         if($title!= ""){
            $this->title = $title;
                    }
         if($body!= ""){
             $this->body = $body;
                    }
               }
     }

    class colorHtmalpage extends Htmalpage{
        protected $colors = array ('red', 'yellow', 'green');
        protected $color = 'red';
        public function __set($property,$value){
            if ($property == 'color'){
                if (in_array($value, $this->colors)){
                    $this->color = $value;
                }
                else{
                    die("you choose NA color"); 
                }
            }
        }
        public function show(){
            echo "<html><head><title>$this->title</title></head><body><p style = 'color:$this->color'>$this->body<p></body></html>";
        }
    }

    class EX3 extends colorHtmalpage{
        protected $font = 14;
        public function __set($property, $value){
            if ($property == 'color'){
                     parent::__set($property, $value);
            }elseif ($property == 'font'){
                if ($value >=10 && $value <=24){
                    $this->font = $value;
                } else {
                    die("You choose NA font-size"); 
                }
            }
        }     
        public function show(){
            echo "<html><head><title>$this->title</title></head><body><p style = 'font-size:$this->font; color:$this->color'>$this->body<p></body></html>";
        }
    }  
?>
